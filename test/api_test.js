const app = require('../app');
const assert = require('assert');
const request = require('supertest');
const farmers = require('../lib/farmers');

describe('Farmers Api', () => {
    describe('farmers search', () => {
        it('Returns all farmers if no parameter is sent', (done) => {
           request(app)
                .get('/farmers')
                .expect('Content-Type', /json/)
                .expect((response) => {
                    console.log(response.body.length == 3);
                })
                .expect(200, done)

        });

        it('Returns farmers wich name starts with parameter', (done) => {
            const farmer = farmers[2];
            request(app)
                .get(`/farmers?q=${farmer.name}`)
                .expect('Content-Type', /json/)
                .expect((response) => {
                    console.log(response.body.length == 1);
                    assert.equal(response.body[0].name, farmer.name);
                })
                .expect(200, done)

        });

        it('Returns farmers wich document number matches parameter', (done) => {
            const farmer = farmers[1];
            request(app)
                .get(`/farmers?q=${farmer.document.documentNumber}`)
                .expect('Content-Type', /json/)
                .expect((response) => {
                    console.log(response.body.length == 1);
                    assert.equal(response.body[0].name, farmer.name);
                })
                .expect(200, done)

        });
    })
});
