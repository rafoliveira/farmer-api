const assert = require('assert');
const farmers = require('../lib/farmers');
const farmersRepository = require('../lib/farmer-repository');

describe('Farmers Repository', () => {
    describe('searchs', () => {
        it('Returns all farmers', () => {
            assert.equal(3, farmersRepository.all().length);
        });

        it('Returns all farmers wich matchs name or document number', () => {
            const name = farmers[0].name;
            const returnedFarmers = farmersRepository.allByNumDocOrName(name);

            assert.equal(1, returnedFarmers.length);

            const foundFarmer = returnedFarmers[0];
            assert.equal(name, foundFarmer.name);
        });

        it('Returns all farmers wich matchs document number', () => {
            const number = farmers[2].document.documentNumber;
            const returnedFarmers = farmersRepository.allByNumDocOrName(number);

            assert.equal(1, returnedFarmers.length);

            const foundFarmer = returnedFarmers[0];
            assert.equal(number, foundFarmer.document.documentNumber);
        });

    })
});
