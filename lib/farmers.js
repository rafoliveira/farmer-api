const addresses = [
    {
        street: "calle X #60a-03",
        state: "Bogota",
        country: "CO",
    },
    {
        street: "calle Y #50a-03",
        state: "Bogota",
        country: "CO",
    },
    {
        street: "calle Z #40a-03",
        state: "Bogota",
        country: "CO",
    },
];
const documents = [
    {
        documentNumber: "a998877",
        documentType: "ID",
    },
    {
        documentNumber: "5544332211",
        documentType: "DRIVE_LICENSE",
    },
    {
        documentNumber: "12345678",
        documentType: "ID",
    },
];


const farmers = [
    {
        id: 1,
        name: "Rafael Oliveira",
        document: documents[0],
        address: addresses[0],
    },
    {
        id: 2,
        name: "Carolina Suica",
        document: documents[1],
        address: addresses[1],
    },
    {
        id: 3,
        name: "Limao Leite",
        document: documents[2],
        address: addresses[2],
    }
]


module.exports = farmers;
