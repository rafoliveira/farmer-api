const farmers = require('./farmers');

const all = () => {
    return farmers;
}

const allByNumDocOrName = (term) => {
    return farmers.filter(farmer => {
        return (farmer.name.toLowerCase().startsWith(term.toLowerCase())) || (farmer.document.documentNumber == term);
    });
}


module.exports = {
    all,
    allByNumDocOrName
}
