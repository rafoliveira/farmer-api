const express = require('express');
const cors = require('cors');
const app = express();
app.use(cors());

const farmerRepository = require('./lib/farmer-repository');

app.get('/', (req, res) => {
    res.send('it Works!!');
});

app.get('/farmers', (req, res) => {
    if(req.query.q){
        return res.send(farmerRepository.allByNumDocOrName(req.query.q));
    }
    return res.send(farmerRepository.all());
});

module.exports = app;
